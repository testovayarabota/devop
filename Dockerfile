FROM python:latest
MAINTAINER Artem Treg "temasuper@gmail.com"
WORKDIR /app 
COPY . /app
RUN pip install -r requirements.txt
CMD python app.py
EXPOSE 5000
